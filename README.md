# DECODER Reverse proxy
This section should be moved to a dedicated repository on time. For now the reverse proxy is embedded in the frontend container but in the long term it should be independent (if we keep a reverse-proxy approach).

The reverse proxy mission is to route requests from the Internet to either the DECODER tools - as REST APIs or webapps - or to the frontend. It is the single entry point (of failure) for browsers.

The current setup leverages two domain names:
- `https://decoder-tool.<domain>` (backend tools)
- `https://decoder-frontend.<domain>` (frontend)

They are certified through let's encrypt certification authority.

## Prefix and container
Tools are mapped to their related backend container through NGINX using a `location` prefix matching pattern, except for `/pkm` which use a dedicated `location` match in order to preserve [URL-encoding](https://en.wikipedia.org/wiki/Percent-encoding).

The matching prefix is associated to a container using a dynamic `map` (see [decoder.conf.template](docker/decoder.conf.template))

# Docker Compose
See [docker-compose.yml](docker-compose.yml)

# BootStrap nginx certificates

On first start, the letsencrypt mounted folder is expected to be empty. It is needed to request the certificates.

ℹ️ nginx configuration for both domains at [docker/decoder.conf.template](docker/decoder.conf.template) listen on Non-SSL port 8500 *only*. This port should **not** be exposed to the Internet.

The following step remains to do in order to add official certificates to both domains:

    docker-compose exec decoder-reverse-proxy certbot --domains decoder-tool.<domain>,decoder-frontend.<domain> -n --nginx --agree-tos -m <renewal_email>
