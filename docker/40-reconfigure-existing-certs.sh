#!/bin/sh

certbot -n --nginx --domains decoder-tool.ow2.org,decoder-frontend.ow2.org,decoder-demo-tool.ow2.org,decoder-demo-frontend.ow2.org
nginx -s stop && sleep 1
